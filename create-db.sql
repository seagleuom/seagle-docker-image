
create table hibernate_sequence
(
	next_val bigint null
);

create table bad_smell
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	description longtext not null,
	name varchar(1024) not null
);

create table developer
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	email varchar(255) not null,
	name varchar(256) null,
	constraint UK_chmq1wdfnhjthxo65affwdnky
		unique (email)
);

create table metric_category
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	categoryName varchar(255) not null,
	description longtext null,
	constraint UK_gysec9bki5a7e05txfywe9jsr
		unique (categoryName)
);

create table metric
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	description longtext null,
	implementationClass varchar(255) not null,
	mnemonic varchar(255) not null,
	name varchar(255) not null,
	metric_category_id bigint not null,
	constraint UK_7b8l5nmr937kqr5k2mhfhnsof
		unique (mnemonic),
	constraint FKwtf9el1aj5o9pde7vogfiq5g
		foreign key (metric_category_id) references metric_category (id)
);

create table metric_dependency
(
	metric_id bigint not null,
	metric_dependency_id bigint not null,
	constraint FKl3cnac53kd7nxfl9s6rdy3yds
		foreign key (metric_dependency_id) references metric (id),
	constraint FKnp7mc464de72f6fdtwlcf8ccp
		foreign key (metric_id) references metric (id)
);

create table metric_value
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	metric_id bigint not null,
	node_id bigint null,
	project_id bigint null,
	value double not null,
	version_id bigint null
);

create table programming_language
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	name varchar(100) not null
);

create table language_applied
(
	prog_lang_id bigint not null,
	metric_id bigint not null,
	constraint FK5g7jm20ex690j0o3rbh75cixt
		foreign key (prog_lang_id) references programming_language (id),
	constraint FK8otdotfbgdcfftg9386kpq05o
		foreign key (metric_id) references metric (id)
);

create table project
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	description longtext null,
	imagePath longtext null,
	name varchar(255) not null,
	remoteRepoPath varchar(255) null,
	constraint UK_3k75vvu7mevyvvb5may5lj8k7
		unique (name),
	constraint UK_46uv9acwoatou3q3b23nbti6k
		unique (remoteRepoPath)
);

create table project_info
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	date_created datetime(6) null,
	date_inserted datetime(6) null,
	lastCommitID varchar(1024) null,
	last_update datetime(6) null,
	project_id bigint not null,
	constraint UK_bvtnw8dekf2y9gbxt7thib8vj
		unique (project_id),
	constraint FKs1uef3fl2ftkgkjk8dmihrxjj
		foreign key (project_id) references project (id)
);

create table project_language
(
	programming_language_id bigint not null,
	project_id bigint not null,
	constraint FKj9u225we27islp3g8lg52kx3b
		foreign key (project_id) references project (id),
	constraint FKnamy5w8e8hkjy6ry6x89yvsfo
		foreign key (programming_language_id) references programming_language (id)
);

create table project_metric
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	metricValue_id bigint not null,
	project_id bigint not null,
	registeredMetrics_id bigint not null,
	constraint UK_qi1sqln6accqo7e3pii2syayc
		unique (metricValue_id),
	constraint FK2cs137p7e3onaw52590r00ld1
		foreign key (project_id) references project (id),
	constraint FKg813yoc8n7n4nlp9cw6ulkmfx
		foreign key (metricValue_id) references metric_value (id),
	constraint FKmmw564lmy99ymowibnqpybp5y
		foreign key (registeredMetrics_id) references metric (id)
);

create table project_owner
(
	developer_id bigint not null,
	project_id bigint not null,
	constraint FK5iy9lk0uau6523pkp9ei9bk64
		foreign key (project_id) references project (id),
	constraint FKh2y45muy8wog5s4itd0c28fm0
		foreign key (developer_id) references developer (id)
);

create table project_registered_metrics
(
	metric_id bigint not null,
	project_id bigint not null,
	constraint FKe84gwjt1lfurlak4op94u7uhw
		foreign key (metric_id) references metric (id),
	constraint FKll0hxg0gs5w8ux7788sib9x15
		foreign key (project_id) references project (id)
);

create table project_timeline
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	actionType varchar(30) null,
	created datetime(6) not null,
	updated datetime(6) not null,
	project_id bigint not null,
	constraint FK9an5wb90dyvldhk1wfqb2gx3k
		foreign key (project_id) references project (id)
);

create table property
(
	id varchar(64) not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	domain varchar(255) not null,
	name varchar(4096) not null,
	value varchar(4096) not null
);

create table version
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	analyzed bit null,
	commitID varchar(1024) not null,
	date date null,
	name varchar(256) not null,
	project_id bigint not null,
	constraint FK5q7csydn4alo2pf0bbv74g9ko
		foreign key (project_id) references project (id)
);

create table node
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	name varchar(512) not null,
	simpleName varchar(255) not null,
	sourceFilePath varchar(1024) not null,
	version_id bigint not null,
	constraint FK5ch6ydkrowxpjo4tvadodfhvb
		foreign key (version_id) references version (id)
);

create table method
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	name varchar(1000) not null,
	simpleName varchar(100) not null,
	node_id bigint null,
	constraint FKr172yxypfthwmmw0js3lfweeu
		foreign key (node_id) references node (id)
);

create table method_metric_value
(
	Method_id bigint not null,
	metricValues_id bigint not null,
	constraint UK_gbjptswx6cku2jh6h8bx1pda5
		unique (metricValues_id),
	constraint FKbvblg3rlfgcejnen1s2kggfcl
		foreign key (metricValues_id) references metric_value (id),
	constraint FKgp8hcfkgnbfdf09og1j56iaxo
		foreign key (Method_id) references method (id)
);

create table method_smells
(
	method_id bigint not null,
	smell_id bigint not null,
	constraint FKel6dgb955dfrtxnox3w10ukv7
		foreign key (smell_id) references bad_smell (id),
	constraint FKlhieuxknloi4qmt9q6wruqogs
		foreign key (method_id) references method (id)
);

create table node_smells
(
	node_id bigint not null,
	smell_id bigint not null,
	constraint FKino4w46kjibykw9p935n4976f
		foreign key (smell_id) references bad_smell (id),
	constraint FKtjr7ibjq7bxjbpnjgm4xg6slh
		foreign key (node_id) references node (id)
);

create table project_version_metric
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	metricValue_id bigint not null,
	version_id bigint null,
	constraint UK_8k3rrc2o36yt662jedpnay2he
		unique (metricValue_id),
	constraint FKk8yx54xb3idscfdi04ncfib33
		foreign key (version_id) references version (id),
	constraint FKpqscl3iphsbahthcqi72q5ogf
		foreign key (metricValue_id) references metric_value (id)
);

create table version_author
(
	developer_id bigint not null,
	version_id bigint not null,
	constraint FKe44e4gdyakl2egay7a3uwlf0v
		foreign key (developer_id) references developer (id),
	constraint FKoklyky4p7748ifrwh8vwyly9j
		foreign key (version_id) references version (id)
);

create table version_commiter
(
	developer_id bigint not null,
	version_id bigint not null,
	constraint FK4ndlm4m57o4vy4acyiqj4ik7
		foreign key (version_id) references version (id),
	constraint FKigd6a1qs1wt5hurlj0il1p9wm
		foreign key (developer_id) references developer (id)
);

create table version_graph
(
	id bigint not null
		primary key,
	CREATED_BY varchar(30) null,
	CREATION_DATE timestamp null,
	LAST_UPDATED_DATE timestamp null,
	STATUS varchar(1) null,
	uuid varchar(40) null,
	jsonGraph longtext null,
	version_id bigint not null,
	constraint UK_1r9g6oq7h54qi02661h0ffov8
		unique (version_id),
	constraint FKsen8obu7o2viwlo9vtqxfpqns
		foreign key (version_id) references version (id)
);

