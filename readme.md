## Docker content for seagle
 Contains:  

  - a Dockerfile with environment variables for the Wildfly Application Server and the MySql Database Contains also instructions for the initial setup of the Wildfly Server (datasource creation)  
  
  - docker-compose.yml for the creation of 2 necessary services: the DB and the Application Server  

to fire it up simply run:

```docker-compose up```

this will attach the docker console to the current active terminal, and if you close it you will terminate the servers.

if you want to run containers on detached mode (ie. to continue work even after you close the terminal), use:

```docker-compose up -d```

then you can monitor log files with:

```docker logs -f seagle-server```


# Seagle REST API

### You can study the seagle api in the [Public API Documentation Page](https://app.swaggerhub.com/apis-docs/Sea95/Seagle/1.0.0)

Example usage:

first you need to get the repo url of a desired project. For example for [scribe-java](https://github.com/scribejava/scribejava.git) the url is:  
``` https://github.com/scribejava/scribejava.git```

Then you must register this project in seagle engine to get a proper id:

```curl --location --request POST 'http://localhost:8080/seagle3/rs/project?requestorEmail=teohaik@gmail.com&purl=https://github.com/scribejava/scribejava.git'```

response:

```
{
    "id": 2301,
    "name": "scribejava",
    "remoteRepoPath": "https://github.com/scribejava/scribejava.git",
    "createdBy": "teohaik@gmail.com",
    "dateCreated": "2020-09-17T20:45:23.967674",
    "updated": null,
    "analyzed": null,
    "versionCount": null,
    "versions": [],
    "metrics": [],
    "softwareProjects": []
}
```

After project registration, seagle engine clones the remote repo and tries to determine project versions
#### If the project repository contains tags, then the versions will be deduced from the tags. Otherwise no versions can be determined and you have to provide some selected commits to be used as versions

You can check if the version determination finished by calling:

```curl --location --request GET 'http://localhost:8080/seagle3/rs/project/versions/all?&purl=https://github.com/scribejava/scribejava.git' ```

(described also [here](https://app.swaggerhub.com/apis-docs/Sea95/Seagle/1.0.0#/versions/getVersionsWS) )

```
{
    "versions": [
        {
            "versionDbId": 2302,
            "commitHashId": "129d368d923e5af1a3e9e0764a035a9eb5fa2817",
            "versionName": "1.0.1",
            "created": 1283990400000,
            "nodeCount": null,
            "metrics": [],
            "nodeList": [],
            "name": "1.0.1",
            "date": 1283990400000
        },
        {
            "versionDbId": 2303,
            "commitHashId": "b2147de2e52d1128ad45349b68b639b693cde405",
            "versionName": "1.0.2",
            "created": 1284508800000,
            "nodeCount": null,
            "metrics": [],
            "nodeList": [],
            "name": "1.0.2",
            "date": 1284508800000
        }
        ....
        ....
]
```

After retrieving the version you can ask for an evolution analysis and metric calculation by calling:

```
curl --location --request POST 'http://localhost:8080/seagle3/rs/project/analysis' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 25,
    "name": "evolutionDemo",
    "remoteRepoPath": "https://github.com/teohaik/evolutionDemo.git",
    "createdBy": "teohaik@gmail.com",
    "dateCreated": "2020-09-07T20:42:22.267629",
    "updated": null,
    "analyzed": null,
    "versionCount": null,
    "versions": [
      {  "versionDbId": 26,  "commitHashId": "e488f7667e8e839a5a9d1023da451930b6bd93ba"},
      {  "versionDbId": 30,  "commitHashId": "a63921227806796d4a10117513a0a1b22808d353"},
      {  "versionDbId": 32,  "commitHashId": "709c319a395f3ef939c3adbdea92c5fda8c8262d"},
      {  "versionDbId": 34,  "commitHashId": "c80dfe2c20f1d7033ccd75ba5c708cff63f7274f"},
      {  "versionDbId": 36,  "commitHashId": "83005f0d2a208049bbe0d8dc293c92f538c2095e"}
    ],
    "metrics": []
}'
``` 

(described also [here](https://app.swaggerhub.com/apis-docs/Sea95/Seagle/1.0.0#/analysis/requestAnalysis) )

Example POST Body (JSON): 

```
{
    "id": 25,
    "name": "evolutionDemo",
    "remoteRepoPath": "https://github.com/teohaik/evolutionDemo.git",
    "createdBy": "teohaik@gmail.com",
    "dateCreated": "2020-09-07T20:42:22.267629",
    "updated": null,
    "analyzed": null,
    "versionCount": null,
    "versions": [
        {  "versionDbId": 26,  "commitHashId": "e488f7667e8e839a5a9d1023da451930b6bd93ba"},
        {  "versionDbId": 30,  "commitHashId": "a63921227806796d4a10117513a0a1b22808d353"},
        {  "versionDbId": 32,  "commitHashId": "709c319a395f3ef939c3adbdea92c5fda8c8262d"},
        {  "versionDbId": 34,  "commitHashId": "c80dfe2c20f1d7033ccd75ba5c708cff63f7274f"},
        {  "versionDbId": 36,  "commitHashId": "83005f0d2a208049bbe0d8dc293c92f538c2095e"}
    ],
    "metrics": []
}
```  

The versions array contains the database IDs and commit Hashes of the desired versions that you require to be analyzed


# Metrics API

After successful analysis, all metrics are available through the API. 

## Examples:

  ### Get all project-level metrics for a project:

  ```curl --location --request GET 'http://localhost:8080/seagle3/rs/metric/values/project/evolutionDemo'```

  ### Get Coupling between objects (CBO) metric for a version of a class:

  ```curl --location --request GET 'http://localhost:8080/seagle3/rs/metric/single/values/project/forVersion?purl=https://github.com/teohaik/evolutionDemo.git&metricMnemonic=CBO&versionID=709c319a395f3ef939c3adbdea92c5fda8c8262d'``` 

  ### Get all class level metric values for a project

  ```curl --location --request GET 'http://localhost:8080/seagle3/rs/metric/values/project/PROJECT_NAME' ``` 


  